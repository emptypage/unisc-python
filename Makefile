UCD_BASE_URL = http://www.unicode.org/Public/10.0.0
UNISC_DATA_DIR = src/unisc/data
UCD_DATA_FILES = \
 $(UNISC_DATA_DIR)/PropertyValueAliases.txt \
 $(UNISC_DATA_DIR)/ScriptExtensions.txt \
 $(UNISC_DATA_DIR)/Scripts.txt
CURL_FLAGS = --compressed

download: $(UCD_DATA_FILES)

$(UNISC_DATA_DIR)/PropertyValueAliases.txt: $(UNISC_DATA_DIR)
	curl $(CURL_FLAGS) -o $@ $(UCD_BASE_URL)/ucd/PropertyValueAliases.txt

$(UNISC_DATA_DIR)/ScriptExtensions.txt: $(UNISC_DATA_DIR)
	curl $(CURL_FLAGS) -o $@ $(UCD_BASE_URL)/ucd/ScriptExtensions.txt

$(UNISC_DATA_DIR)/Scripts.txt: $(UNISC_DATA_DIR)
	curl $(CURL_FLAGS) -o $@ $(UCD_BASE_URL)/ucd/Scripts.txt

$(UNISC_DATA_DIR):
	mkdir $@
