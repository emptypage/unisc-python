=====
unisc
=====

The unisc package is a Unicode Script property library for Python.


Features
--------

The library provides:

- Functions to get the Script / Script_Extensions property values of each
  Unicode character.


References
----------

- `UAX #24: Unicode Script Property <http://www.unicode.org/reports/tr24/>`_


Related Projects
----------------

- The `uniscripts <https://github.com/leoboiko/uniscripts>`_ library is designed
  for the same purpose of this library, which is maintained by Leonardo Boiko.
- `PyICU <https://github.com/ovalhub/pyicu>`_ by Andi Vajda is the Python
  library that wraps the International Components for Unicode library (ICU).
