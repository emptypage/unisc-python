"""Setup script for the package. """

from os import path
from setuptools import setup


with open(path.join(path.dirname(__file__), 'README.rst')) as f:
    LONG_DESCRIPTION = f.read()


setup(
    name='unisc',
    version='0.1',
    description='Unicode Script property',
    long_description=LONG_DESCRIPTION,
    url='https://bitbucket.org/emptypage/unisc-python',
    author='Masaaki Shibata',
    author_email='mshibata@emptypage.jp',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Internationalization',
        'Topic :: Text Processing',
    ],
    keywords='property script script_extensions text unicode',
    python_requires='>=3.5',
    packages=['unisc'],
    package_dir={'': 'src'},
    package_data={'': ['data/*']},
)
